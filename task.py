import nltk
import matplotlib.pyplot as plt
# nltk.download('punkt')
text = """Welcome to cloudMind. Lets start with cloudMind"""

# from nltk.tokenize import word_tokenize
# print(word_tokenize(text))

# from nltk.tokenize import sent_tokenize
# print(sent_tokenize(text))

from nltk.tokenize import word_tokenize
word_tokenize = word_tokenize(text)
from nltk.probability import FreqDist
fd = FreqDist(word_tokenize)
print(fd.most_common(6))
fd.plot(30, cumulative=False)
plt.show()