import nltk
# nltk.download('stopwords')

text = """The materials used in building a chatbot can vary depending on the specific requirements and goals of the project. However, some common materials and tools used to build chatbots include:"""
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))
# print(stop_words)

from nltk.tokenize import word_tokenize, sent_tokenize
tokenize_words = word_tokenize(text)
# print (words)

tokenize_words_without_stop_words = []
for word in tokenize_words:
    if word not in stop_words:
        tokenize_words_without_stop_words.append(word)
print(set(tokenize_words)-set(tokenize_words_without_stop_words))
print(tokenize_words)
print(tokenize_words_without_stop_words)
