import nltk
nltk.download('omw-1.4')
nltk.download('vader_lexicon')
from nltk.stem import PorterStemmer, WordNetLemmatizer

# text = ["running","seeing","gave","giving","holder", "holding"]
# lemmatizer = WordNetLemmatizer()
# stemmer = PorterStemmer()
# for word in text:
#     print(word,":", stemmer.stem(word), lemmatizer.lemmatize(word, "v"))

from nltk.sentiment import SentimentIntensityAnalyzer

sia = SentimentIntensityAnalyzer()
print(sia.polarity_scores("How can I kill him"))